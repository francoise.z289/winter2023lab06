public class Board{
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	public Board(){
		this.die1=new Die();
		this.die2=new Die();
		this.tiles=new boolean[12];
	}
	public String toString(){
		for(int showLoop=0;showLoop<this.tiles.length; showLoop++){
			if(tiles[showLoop]==false){
				System.out.print(showLoop+1+" ");
			}else{
				System.out.print("X ");
			}
		}
		return "";
	}
	public boolean playATurn(){
		this.die1.roll();
		this.die2.roll();
		boolean result=false;
		System.out.println("die 1: "+ this.die1.getFaceValue()+"\n die 2: "+this.die2.getFaceValue());
		int sumOfDice=this.die1.getFaceValue()+this.die2.getFaceValue();
		if(this.tiles[sumOfDice-1]){
			//check sum
			if(this.tiles[this.die1.getFaceValue()-1]){
				//check die1
				if(this.tiles[this.die2.getFaceValue()-1]){
					//check die2
					System.out.println("All the tiles fot these values are already shut.");
					result=true;
				}else{
					this.tiles[this.die2.getFaceValue()-1]=true;
					System.out.println("Closing tile with the same value as die two:"+this.die2.getFaceValue());
				}
			}else{
				this.tiles[this.die1.getFaceValue()-1]=true;
				System.out.println("Closing tile with the same value as die one:"+this.die1.getFaceValue());
			}
		}else{
			System.out.println("Closing tile equal to sum"+(sumOfDice));
			this.tiles[sumOfDice-1]=true;
		}
		return result;
	}
}