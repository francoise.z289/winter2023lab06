import java.util.Scanner;
import java.util.ArrayList;
public class Jackpot{
	public static void main(String[]args){
		System.out.println("=======================");
		System.out.println("Hello, welcome to Jackpot Game!\n The rule of the game:\nYou throws 2 dies and calculates the sum of the face values of the two dices.\n Then, the game shut the tile equal to the sum. \n If the tile equals to sum is already shut, the game shut the tiles equals to face value of the die one.\nIf the dies equals to the face value of die 1 is already shut, the game shut the tile equals to the face value of die two.\n If the tile equals to the die 2 is already shuts, the game is over.\n If you shut 7 or more tiles, you win. If not, you lose.");
		System.out.println("=======================");
		int numOfGame=0;
		int win=0;
		ArrayList<Board> boards=new ArrayList<Board>();
		Scanner reader=new Scanner(System.in);
		boolean programOver=false;
		boolean gameOver=false;
		while(programOver==false){
			//multiple game
			gameOver=false;
			boards.add(new Board());
			int numOfTilesClosed=0;
			while(gameOver==false){
				//just one game
				System.out.println(boards.get(numOfGame));
				gameOver=boards.get(numOfGame).playATurn();
				System.out.println("-----------");
				if(gameOver==false){
					numOfTilesClosed++;
				}
			}
			if(numOfTilesClosed>=7){
				System.out.println("You close "+numOfTilesClosed+" tiles. You wins!");
				win++;
			}else{
				System.out.println("You lose! You just close "+numOfTilesClosed+" tiles.");
			}
			System.out.println("Do you want to start a new game?\n Type yes or no");
			String answer=reader.next();
			if(answer.equals("yes")){
				numOfGame++;
			}else{
				programOver=true;
			}
			System.out.println("=======================");
		}
		System.out.println("You win "+win+" times in "+numOfGame+" games.");
	}
}