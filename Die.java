import java.util.Random;
public class Die{
	private int faceValue;
	private Random roll;
	public Die(){
		this.faceValue=1;
		this.roll=new Random();
	}
	public int getFaceValue(){
		return this.faceValue;
	}
	public int roll(){
		return this.faceValue=roll.nextInt(6)+1;
	}
}